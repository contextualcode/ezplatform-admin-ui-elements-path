const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/plugins/elements-path.js'),
        ]
    });
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-richtext-onlineeditor-css',
        newItems: [
            path.resolve(__dirname, '../public/scss/alloyeditor/plugins/elements-path.scss'),
        ]
    });
};
